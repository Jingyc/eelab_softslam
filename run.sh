#!/bin/bash

SLAM_PATH=${0%/*}

${SLAM_PATH}/build/bin/soft_slam --images-left-path=${SLAM_PATH}/data --images-right-path=${SLAM_PATH}/data
