#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <vector>

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <opencv2/core/eigen.hpp>
#include <opencv2/opencv.hpp>

using namespace std;

struct CAMERA_INTRINSIC_PARAMETERS {
  double cx, cy, fx, fy, scale;
};

struct Frame {
  int frameID;
  cv::Mat left, right;
  cv::Mat desp;
  vector<cv::KeyPoint> kp;
  vector<cv::DMatch> match;
};

struct FeatureManagement {
  int id;
  int age = 0;
  double strength;
  int clas;
  cv::Mat desc;
  cv::Point2f p_l;
  cv::Mat firstPatch;
};

struct KeyPoints {
  cv::Point position;
  double strength;
};

class CornerDetector {
public:
  cv::Mat cornerStrength;
  cv::Mat cornerTheshold;
  cv::Mat localMax;

  int neighbourPixelSize;
  int aperture;
  double k;

  double minStrength;
  double maxStrength;
  double threshold;
  int noneighbourPixelSize;
  cv::Mat kernel;

public:
  CornerDetector() : neighbourPixelSize(5), aperture(3), k(0.01), maxStrength(0.0), threshold(0.01), noneighbourPixelSize(3) {
    setLocalMaxWindowSize(noneighbourPixelSize);
  }

  void setLocalMaxWindowSize(int size);
  void detect(const cv::Mat & image);
  cv::Mat getCornerMap(double qualityLevel);
  void getCorners(vector<KeyPoints> & points, double qualityLevel);
  void getCorners(vector<KeyPoints> & points, const cv::Mat cornerMap);
  cv::Mat drawOnImage(cv::Mat & image, const vector<KeyPoints> & points, cv::Scalar color = cv::Scalar(255, 255, 255), int radius = 4, int thickness = 2);
};

class ParameterReader {
public:
  ParameterReader(string filename = "/home/caojingyi/soft_slam/parameters.txt") {
    ifstream fin(filename.c_str());
    if (!fin) {
      cerr << "parameter file does not exist." << endl;
      return;
    }

    while (!fin.eof()) {
      string str;
      getline(fin, str);
      if (str[0] == '#') {
        continue;
      }

      int pos = str.find("=");
      if (pos == -1)
        continue;
      string key = str.substr(0, pos);
      string value = str.substr(pos + 1, str.length());
      data[key] = value;

      if (!fin.good())
        break;
    }
  }

  string getData(string key) {
    map<string, string>::iterator iter = data.find(key);
    if (iter == data.end()) {
      cerr << "Parameter name " << key << "not found!" << endl;
      return string("NOT_FOUND");
    }
    return iter->second;
  };

public:
  map<string, string> data;
};

inline static CAMERA_INTRINSIC_PARAMETERS getDefaultCamera() {
  ParameterReader pd;
  CAMERA_INTRINSIC_PARAMETERS camera;
  camera.fx = atof(pd.getData("camera.fx").c_str());
  camera.fy = atof(pd.getData("camera.fy").c_str());
  camera.cx = atof(pd.getData("camera.cx").c_str());
  camera.cy = atof(pd.getData("camera.cy").c_str());
  camera.scale = atof(pd.getData("camera.scale").c_str());
  return camera;
}

//int id = 0;
Frame readFrame(int index, ParameterReader & pd);
void creatKern(vector<cv::Point> & k);
void creatRefinePatch(cv::Point p, cv::Mat left, cv::Mat & patch);
void preFeatureMatch(vector<int> & matchIdx, cv::Mat & left, cv::Mat & right, vector<KeyPoints> & kp1, vector<KeyPoints> & kp2, vector<cv::Point> k);
int preFeatureMatch(cv::Mat & left, cv::Mat & right, cv::Point p, cv::Point q, vector<cv::Point> k);
void NCC(cv::Mat & left, cv::Mat & right, float & s);
void circleMatch(vector<int> & matchIdx1, vector<int> & matchIdx2, vector<int> & matchIdx3, vector<int> & matchIdx4, vector<KeyPoints> kp1, vector<KeyPoints> kp2, vector<FeatureManagement> & currfeatures, vector<int> & currKp2Feature, cv::Mat left, cv::Mat lastLeft, int & id);
void circleMatch(vector<int> & matchIdx1, vector<int> & matchIdx2, vector<int> & matchIdx3, vector<int> & matchIdx4, vector<KeyPoints> kp1, vector<KeyPoints> kp2, vector<FeatureManagement> lastfeatures, vector<int> lastKp2Feature, vector<FeatureManagement> & currfeatures, vector<int> & currKp2Feature, cv::Mat left, cv::Mat lastLeft, int & id);
void positionRefine(vector<FeatureManagement> & currfeatures, cv::Mat left);

void featureClass(vector<FeatureManagement> & currfeatures, double min, double max);
void featureSelect(vector<FeatureManagement> & currfeatures_final, vector<FeatureManagement> currfeatures, cv::Mat left, int ageThreshold, vector<int> & featureIdx);

void poseEstimate(vector<FeatureManagement> currfeatures, vector<FeatureManagement> lastfeatures, vector<cv::Point2f> & currpoints, vector<cv::Point2f> & lastpoints, cv::Mat & R, cv::Mat & t, cv::Mat & E_mat);
void poseRefine(cv::Mat & R, cv::Mat & R1, cv::Mat & R_last);
cv::Point2f pixel2cam(const cv::Point2f & p);