#include "slambase.h"
#include <algorithm>
#include <vector>

using namespace std;

void CornerDetector::setLocalMaxWindowSize(int size) {
  noneighbourPixelSize = size;
  kernel.create(noneighbourPixelSize, noneighbourPixelSize, CV_8U);
}

void CornerDetector::detect(const cv::Mat & image) {
  cv::cornerHarris(image, cornerStrength, neighbourPixelSize, aperture, k);
  cv::minMaxLoc(cornerStrength, &minStrength, &maxStrength);
  cv::Mat dilate; // 临时图像
  cv::dilate(cornerStrength, dilate, cv::Mat());
  cv::compare(cornerStrength, dilate, localMax, cv::CMP_EQ);
}

cv::Mat CornerDetector::getCornerMap(double qualityLevel) {
  cv::Mat cornerMap;
  threshold = qualityLevel * maxStrength;
  cv::threshold(cornerStrength, cornerTheshold, threshold, 255, cv::THRESH_BINARY);
  cornerTheshold.convertTo(cornerMap, CV_8U); //转化为8位图像
  cv::bitwise_and(cornerMap, localMax, cornerMap);
  return cornerMap;
}

void CornerDetector::getCorners(vector<KeyPoints> & points, double qualityLeval) {
  cv::Mat cornerMap = getCornerMap(qualityLeval);
  getCorners(points, cornerMap);
}

void CornerDetector::getCorners(vector<KeyPoints> & points, const cv::Mat cornerMap) {
  for (int y = 0; y < cornerMap.rows; y++) {
    const uchar * rowPtr = cornerMap.ptr<uchar>(y);
    for (int x = 0; x < cornerMap.cols; x++) {
      if (rowPtr[x]) {
        KeyPoints p;
        p.position = cv::Point(x, y);
        p.strength = cornerStrength.ptr<float>(y)[x];
        points.push_back(p);
      }
    }
  }
}

cv::Mat CornerDetector::drawOnImage(cv::Mat & image, const vector<KeyPoints> & points, cv::Scalar color, int radius, int thickness) {
  cv::Mat imgWithKp = image;
  for (int i = 0; i < points.size(); i++) {
    cv::circle(imgWithKp, points[i].position, radius, color, thickness);
  }
  return imgWithKp;
}
