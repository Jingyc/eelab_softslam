#include "slambase.h"
//#include <DirectXMath.h>
#include <algorithm>
#include <cmath>
#include <thread>
#include <time.h>


#include <Eigen/Core>
#include <Eigen/Geometry>
#include <opencv2/core/eigen.hpp>

using namespace std;

ParameterReader pd;
int main(int argc, char ** argv) {
  int startIndex = atoi(pd.getData("start_index").c_str());
  int endIndex = atoi(pd.getData("end_index").c_str());
  double qualityLevel = atof(pd.getData("quality_level").c_str());
  int ageThreshold = atoi(pd.getData("age_threshold").c_str());

  cout << "Initializing..." << endl;
  vector<KeyPoints> lastLeftKp, lastRightKp;
  vector<KeyPoints> kp1, kp2;

  vector<int> lastKp2Feature, currKp2Feature;
  vector<FeatureManagement> lastfeatures, currfeatures, currfeatures_final, lastfeatures_final, prefeatures_final;

  cv::Mat E_mat, R, t, R_last, t_last;
  cv::Mat R_orig, R_last_orig, t_orig, t_last_orig;

  int id = 0;

  int lastIndex = startIndex;
  Frame lastFrame = readFrame(lastIndex, pd);
  cv::Mat leftGrey, rightGrey;
  leftGrey = lastFrame.left;
  rightGrey = lastFrame.right;

  CornerDetector harris;
  harris.detect(leftGrey);
  harris.getCorners(lastLeftKp, qualityLevel);
  harris.detect(rightGrey);
  harris.getCorners(lastRightKp, qualityLevel);

  vector<cv::Point> matchKern;
  creatKern(matchKern);

  for (int currIndex = lastIndex + 1; currIndex <= endIndex; currIndex++) {

    clock_t start = clock();

    Frame currframe = readFrame(currIndex, pd);
    Frame lastframe = readFrame(currIndex - 1, pd);
    cv::Mat left = currframe.left;
    cv::Mat right = currframe.right;
    cv::Mat lastl = lastframe.left;
    cv::Mat lastr = lastframe.right;
    cv::Mat dst_l, dst_r;
    dst_l = left;
    dst_r = right;
    //cv::cvtColor(left, dst_l, cv::COLOR_BGR2GRAY);
    //cv::cvtColor(right, dst_r, cv::COLOR_BGR2GRAY);

    CornerDetector harris;
    harris.detect(dst_l);
    harris.getCorners(kp1, qualityLevel);
    double threshold = harris.threshold;
    double maxstrength = harris.maxStrength;

    harris.detect(dst_r);
    harris.getCorners(kp2, qualityLevel);

    clock_t finish = clock();
    double consumetime = (double)(finish - start) / CLOCKS_PER_SEC;
    cout << "feature detection consume " << consumetime << " s." << endl;
    cout << "Key points of two images: " << kp1.size() << "," << kp2.size() << endl;


    //test
    /*cv::Mat imgShow = harris.drawOnImage(dst_l, kp1);
    cv::imwrite("/home/caojingyi/soft_slam/data/keypoints.png", imgShow);*/


    //fetaure match
    vector<int> matchIdx1, matchIdx2, matchIdx3, matchIdx4;
    preFeatureMatch(matchIdx1, dst_l, dst_r, kp1, kp2, matchKern);
    preFeatureMatch(matchIdx2, dst_r, rightGrey, kp2, lastRightKp, matchKern);
    preFeatureMatch(matchIdx3, rightGrey, leftGrey, lastRightKp, lastLeftKp, matchKern);
    preFeatureMatch(matchIdx4, leftGrey, dst_l, lastLeftKp, kp1, matchKern);


    if (currIndex == startIndex + 1) {
      circleMatch(matchIdx1, matchIdx2, matchIdx3, matchIdx4, kp1, lastLeftKp, currfeatures, currKp2Feature, dst_l, leftGrey, id);
    } else {
      circleMatch(matchIdx1, matchIdx2, matchIdx3, matchIdx4, kp1, lastLeftKp, lastfeatures, lastKp2Feature, currfeatures, currKp2Feature, dst_l, leftGrey, id);
    }

    cout << "currfeatures' size is = " << currfeatures.size() << endl;
    positionRefine(currfeatures, dst_l);

    finish = clock();
    consumetime = (double)(finish - start) / CLOCKS_PER_SEC;
    cout << "feature match first consume " << consumetime << " s." << endl;


    //test
    /*vector<cv::DMatch> matches;
    vector<cv::KeyPoint> keyp1, keyp2;
    for (int i = 0; i < kp1.size(); i++) {
      cv::DMatch m;
      m.queryIdx = i;
      m.trainIdx = matchIdx1[i];
      matches.push_back(m);

      cv::KeyPoint k;
      k.pt = kp1[i].position;
      keyp1.push_back(k);
    }
    for (int i = 0; i < kp2.size(); i++) {
      cv::KeyPoint k;
      k.pt = kp2[i].position;
      keyp2.push_back(k);
    }
    cv::Mat imgmatches;
    cv::drawMatches(dst_l, keyp1, dst_r, keyp2, matches, imgmatches);
    cv::imwrite("/home/caojingyi/soft_slam/data/matches.png", imgmatches);

    vector<int> currFeature2Kp(currfeatures.size(), -1);
    for (int i = 0; i < currKp2Feature.size(); i++) {
      if (currKp2Feature[i] != -1) {
        int index = currKp2Feature[i];
        currFeature2Kp[index] = i;
      }
    }
    vector<cv::DMatch> goodmatches;
    for (int i = 0; i < currFeature2Kp.size(); i++) {
      cv::DMatch m;
      m.queryIdx = currFeature2Kp[i];
      m.trainIdx = matchIdx1[currFeature2Kp[i]];
      goodmatches.push_back(m);
    }
    cv::Mat imggood;
    cv::drawMatches(left, keyp1, right, keyp2, goodmatches, imggood);
    cv::imwrite("/home/caojingyi/soft_slam/data/goodmatch.png", imggood);*/



    //feature select
    vector<int> featureIdx;
    featureClass(currfeatures, threshold, maxstrength);
    featureSelect(currfeatures_final, currfeatures, dst_l, ageThreshold, featureIdx);

    finish = clock();
    consumetime = (double)(finish - start) / CLOCKS_PER_SEC;
    cout << "feature select consume " << consumetime << " s." << endl;
    cout << "currfeatures' size is = " << currfeatures_final.size() << endl;



    //test
    /*vector<cv::DMatch> matches_final;
    for (int i = 0; i < currfeatures_final.size(); i++) {
      cv::DMatch m;
      int index = currFeature2Kp[featureIdx[i]];
      m.queryIdx = index;
      m.trainIdx = matchIdx1[index];
      matches_final.push_back(m);
    }
    cv::Mat imgfinal;
    cv::drawMatches(left, keyp1, right, keyp2, matches_final, imgfinal);
    cv::imwrite("/home/caojingyi/soft_slam/data/match_final.png", imgfinal);*/

    //pose estimation
    if (currIndex > startIndex + 1) {
      vector<cv::Point2f> currpoints, lastpoints;
      poseEstimate(currfeatures_final, lastfeatures_final, currpoints, lastpoints, R, t, E_mat);
      if (currIndex == startIndex + 2) {
        R_orig = R;
        t_orig = t;
      }
      //cout << "E: " << E_mat << endl;
    }
    if (currIndex > startIndex + 2) {
      vector<cv::Point2f> currpoints, prepoints;
      cv::Mat R1, t1, E1;
      poseEstimate(currfeatures_final, prefeatures_final, currpoints, prepoints, R1, t1, E1);
      poseRefine(R, R1, R_last);
      R_orig = R_last_orig * R;
      t_orig = t_last_orig + t;
    }
    cout << "R: " << R_orig << endl;
    cout << "t: " << t_orig << endl;
    finish = clock();
    consumetime = (double)(finish - start) / CLOCKS_PER_SEC;
    cout << "pose estimation consume " << consumetime << " s." << endl;


    //clear and update
    leftGrey = dst_l;
    rightGrey = dst_r;
    R_last = R;
    R_last_orig = R_orig;
    t_last_orig = t_orig;

    lastKp2Feature.clear();
    vector<int>::iterator iter;
    for (iter = currKp2Feature.begin(); iter != currKp2Feature.end(); iter++) {
      lastKp2Feature.push_back(*iter);
    }
    currKp2Feature.clear();

    prefeatures_final.clear();
    vector<FeatureManagement>::iterator pre;
    for (pre = lastfeatures_final.begin(); pre != lastfeatures_final.end(); pre++)
      prefeatures_final.push_back(*pre);
    lastfeatures_final.clear();
    vector<FeatureManagement>::iterator finaliter;
    for (finaliter = currfeatures_final.begin(); finaliter != currfeatures_final.end(); finaliter++)
      lastfeatures_final.push_back(*finaliter);
    currfeatures_final.clear();


    lastfeatures.clear();
    vector<FeatureManagement>::iterator it;
    for (it = currfeatures.begin(); it != currfeatures.end(); it++)
      lastfeatures.push_back(*it);
    currfeatures.clear();

    lastLeftKp.clear();
    vector<KeyPoints>::iterator kpt;
    for (kpt = kp1.begin(); kpt != kp1.end(); kpt++) {
      lastLeftKp.push_back(*kpt);
    }
    kp1.clear();

    lastRightKp.clear();
    vector<KeyPoints>::iterator kpt1;
    for (kpt1 = kp2.begin(); kpt1 != kp2.end(); kpt1++)
      lastRightKp.push_back(*kpt1);
    kp2.clear();
  }
}

Frame readFrame(int index, ParameterReader & pd) {
  Frame f;
  string leftDir = pd.getData("left_dir");
  string rightDir = pd.getData("right_dir");

  string leftExt = pd.getData("left_extension");
  string rightExt = pd.getData("right_extension");

  stringstream ss;
  ss << leftDir << index << leftExt;
  string filename;
  ss >> filename;
  f.left = cv::imread(filename, 0);
  if (f.left.channels() != 1)
    cout << " the picture is not grey" << endl;

  filename.clear();
  ss.clear();
  ss << rightDir << index << rightExt;
  ss >> filename;
  f.right = cv::imread(filename, 0);

  return f;
}
