#include "slambase.h"

void pointSeletect(vector<FeatureManagement> currfeatures, vector<FeatureManagement> lastfeatures, vector<cv::Point2f> & currpoints, vector<cv::Point2f> & lastpoints) {
  int cnt = 0;
  for (int i = 0; i < currfeatures.size(); i++) {
    for (int j = 0; j < lastfeatures.size(); j++) {
      if (lastfeatures[j].id == currfeatures[i].id) {
        cv::Point2f p = currfeatures[i].p_l;
        cv::Point2f q = lastfeatures[j].p_l;
        currpoints.push_back(p);
        lastpoints.push_back(q);
        cnt++;
        continue;
      }
    }
  }
}

void poseEstimate(vector<FeatureManagement> currfeatures, vector<FeatureManagement> lastfeatures, vector<cv::Point2f> & currpoints, vector<cv::Point2f> & lastpoints, cv::Mat & R, cv::Mat & t, cv::Mat & E_mat) {
  pointSeletect(currfeatures, lastfeatures, currpoints, lastpoints);
  vector<cv::Point2f> currFivePoitns, lastFivePoints;
  for (int i = 0; i < 8; i++) {
    currFivePoitns.push_back(currpoints[i]);
    lastFivePoints.push_back(lastpoints[i]);
  }
  E_mat = cv::findEssentialMat(lastFivePoints, currFivePoitns, 457.9, cv::Point2f(367.215, 248.375), cv::RANSAC, 0.999, 1.0);
  cv::recoverPose(E_mat, lastpoints, currpoints, R, t, 457.9, cv::Point2f(367.215, 248.375));
}

void poseRefine(cv::Mat & R, cv::Mat & R1, cv::Mat & R_last) {
  Eigen::Matrix3d r, r1;
  R1 = R_last.inv() * R1;
  cv::cv2eigen(R, r);
  cv::cv2eigen(R1, r1);
  Eigen::Quaterniond q = Eigen::Quaterniond(r);
  Eigen::Quaterniond q1 = Eigen::Quaterniond(r1);
  float para = 0.5;
  Eigen::Quaterniond q2 = q.slerp(para, q1);
  r = q.toRotationMatrix();
  cv::eigen2cv(r, R);
}

cv::Point2f pixel2cam(const cv::Point2f & p) {
  cv::Mat K = (cv::Mat_<double>(3, 3) << 458.654, 0, 367.215, 0, 457.296, 248.375, 0, 0, 1);
  return cv::Point2f((p.x - K.at<double>(0, 2)) / K.at<double>(0, 0), (p.y - K.at<double>(1, 2)) / K.at<double>(1, 1));
}

void triangulation(cv::Mat R, cv::Mat t, vector<cv::Point2f> currpoints, vector<cv::Point2f> lastpoints, vector<cv::Point3d> & points) {
  cv::Mat T1 = (cv::Mat_<float>(3, 4) << 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0);
  cv::Mat T2 = (cv::Mat_<float>(3, 4) << R.at<double>(0, 0), R.at<double>(0, 1), R.at<double>(0, 2), t.at<double>(0, 0), R.at<double>(1, 0), R.at<double>(1, 1), R.at<double>(1, 2), t.at<double>(1, 0), R.at<double>(2, 0), R.at<double>(2, 1), R.at<double>(2, 2), t.at<double>(2, 0));
  vector<cv::Point2f> pts_1, pts_2;
  for (int i = 0; i < currpoints.size(); i++) {
    pts_1.push_back(pixel2cam(currpoints[i]));
    pts_2.push_back(pixel2cam(lastpoints[i]));
  }
  cv::Mat pts_4d;
  cv::triangulatePoints(T1, T2, pts_1, pts_2, pts_4d);

  for (int i = 0; i < pts_4d.cols; i++) {
    cv::Mat x = pts_4d.col(i);
    x /= x.at<float>(3, 0);
    cv::Point3d p(
      x.at<float>(0, 0),
      x.at<float>(1, 0),
      x.at<float>(2, 0));
    points.push_back(p);
  }
}