#include "slambase.h"
#include <iostream>
#include <vector>

using namespace std;

void featureClass(vector<FeatureManagement> & currfeatures, double min, double max) {
  double threshold = min + (max - min) / 2;
  for (int i = 0; i < currfeatures.size(); i++) {
    if (currfeatures[i].strength < threshold)
      currfeatures[i].clas = 0;
    else
      currfeatures[i].clas = 1;
  }
}

void featureSelect(vector<FeatureManagement> & currfeatures_final, vector<FeatureManagement> currfeatures, cv::Mat left, int ageThreshold, vector<int> & featureIdx) {

  FeatureManagement f;
  f.age = 0;
  f.strength = 0;
  vector<vector<FeatureManagement>> patchIdx(2, vector<FeatureManagement>(64, f));
  vector<vector<int>> feature2Kp(2, vector<int>(64, -1));
  int col = left.cols;
  int row = left.rows;
  int unitCol = col / 8 + 1;
  int unitRow = row / 8 + 1;

  for (int i = 0; i < currfeatures.size(); i++) {
    cv::Point p = currfeatures[i].p_l;
    int colIdx = p.x / unitCol;
    int rowIdx = p.y / unitRow;
    int index = colIdx * 8 + rowIdx;
    if (currfeatures[i].clas == 0) {
      if (currfeatures[i].age != patchIdx[0][index].age && currfeatures[i].age <= ageThreshold && patchIdx[0][index].age <= ageThreshold) {
        if (currfeatures[i].age > patchIdx[0][index].age) {
          patchIdx[0][index] = currfeatures[i];
          feature2Kp[0][index] = i;
        }
      } else {
        if (currfeatures[i].strength > patchIdx[0][index].strength) {
          patchIdx[0][index] = currfeatures[i];
          feature2Kp[0][index] = i;
        }
      }
    } else if (currfeatures[i].clas == 1) {
      if (currfeatures[i].age != patchIdx[1][index].age && currfeatures[i].age <= ageThreshold && patchIdx[1][index].age <= ageThreshold) {
        if (currfeatures[i].age > patchIdx[1][index].age) {
          patchIdx[1][index] = currfeatures[i];
          feature2Kp[1][index] = i;
        }
      } else {
        if (currfeatures[i].strength > patchIdx[1][index].strength) {
          patchIdx[1][index] = currfeatures[i];
          feature2Kp[1][index] = i;
        }
      }
    }
  }

  for (int i = 0; i < patchIdx.size(); i++) {
    for (int j = 0; j < 64; j++) {
      if (patchIdx[i][j].age != 0) {
        currfeatures_final.push_back(patchIdx[i][j]);
        featureIdx.push_back(feature2Kp[i][j]);
      }
    }
  }
}
