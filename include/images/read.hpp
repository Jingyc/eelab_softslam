
#pragma once

#include "library.hpp"

struct ImagesReadParams {
  operator boost::program_options::options_description() {
    using namespace std;
    namespace po = boost::program_options;

    po::options_description _desc("Images Read Params");
    auto add_option = _desc.add_options();
    add_option("images-left-path", po::value<string>(&left_path), "images left path");
    add_option("images-right-path", po::value<string>(&right_path), "images right path");
    return _desc;
  }

  std::string left_path;
  std::string right_path;
};


struct FrameFilename {
  string l, r, d;
};


class ImagesRead {
public:
  ImagesRead(ImagesReadParams const & params) : _params(params) {}

  void getFilenames(vector<FrameFilename> & frameFilenames) {
    vector<String> left_filenames;
    vector<String> right_filenames;
    glob(_params.left_path + "/*.png", left_filenames);
    glob(_params.right_path + "/*.png", right_filenames);

    frameFilenames.clear();
    for (size_t i = 0; i < min(left_filenames.size(), right_filenames.size()); ++i) {
      frameFilenames.push_back({ left_filenames[i], right_filenames[i], "" });
    }
  }

private:
  ImagesReadParams _params;
};
