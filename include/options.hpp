
#pragma once

#include "library.hpp"

class OptionsParser {
public:
  using _Myt = OptionsParser;

  OptionsParser();

  _Myt & add(boost::program_options::options_description const & desc);

  void parser(int argc, char * argv[]);

  bool operator!() { return _help; }

private:
  struct {
    std::string left_path;
  } _images;

  bool _help;
  boost::program_options::options_description _desc;
};
