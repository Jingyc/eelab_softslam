
#pragma once

#include "library.hpp"

struct ExtractFeaturesParams {
  operator boost::program_options::options_description() {
    using namespace std;
    namespace po = boost::program_options;

    po::options_description _desc("Extract Features Params");
    auto add_option = _desc.add_options();
    return _desc;
  }
};


class ExtractFeatures {
public:
  ExtractFeatures(ExtractFeaturesParams const & params) : _params(params) {}

private:
  vector<Mat> _lefts;

  ExtractFeaturesParams _params;
};
