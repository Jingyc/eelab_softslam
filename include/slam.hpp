
#pragma once

#include "library.hpp"

#include "options.hpp"

#include "extract.hpp"
#include "images/read.hpp"

class Slam {
public:
  Slam(ImagesReadParams const & imagesReadParams, ExtractFeaturesParams const & extractFeaturesParams) {
    _params.imagesReadParams = imagesReadParams;
  }

  void process() {
    ImagesRead imagesRead(_params.imagesReadParams);

    vector<FrameFilename> frameFilenames;
    imagesRead.getFilenames(frameFilenames);

    for (auto frameFilename : frameFilenames) {
      cout << frameFilename.l << ", " << frameFilename.r << endl;
      auto left = imread(frameFilename.l);
      auto right = imread(frameFilename.r);
    }
  }

private:
  struct {
    ImagesReadParams imagesReadParams;
  } _params;
};
