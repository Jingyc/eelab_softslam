
#include "slam.hpp"

using namespace std;
using namespace cv;

int main(int argc, char * argv[]) {
  cout << "Hello SLAM" << endl;

  ImagesReadParams imagesReadParams;
  ExtractFeaturesParams extractFeaturesParams;

  OptionsParser op;
  op.add(imagesReadParams);
  op.add(extractFeaturesParams);

  op.parser(argc, argv);
  if (!op) {
    // cerr << "return 1" << endl;
    return 1;
  }

  Slam slam(imagesReadParams, extractFeaturesParams);
  slam.process();

  return 0;
}