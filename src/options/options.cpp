
#include "options.hpp"

using namespace std;
namespace po = boost::program_options;

OptionsParser::OptionsParser() : _desc("Options") {
  auto add_option = _desc.add_options();
  add_option("help", "show this help");
}

OptionsParser::_Myt & OptionsParser::add(boost::program_options::options_description const & desc) {
  _desc.add(desc);
  return *this;
}

void OptionsParser::parser(int argc, char * argv[]) {
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, _desc), vm);
  po::notify(vm);

  _help = vm.count("help") != 0;
  if (_help) cout << _desc << endl;
}
